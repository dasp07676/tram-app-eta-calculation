const express = require("express");
var http = require('http');
const https = require('https');
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require('path');
const fs = require('fs');


var debug = require('debug')('smart-trum:server');
var logger = require('morgan');

//const fileUpload = require('express-fileupload');
//const webSocketServer = require("websocket").server;

const app = express();

global.__basedir = __dirname + "/";

var corsOptions = {
    // origin: "http://localhost:8081"
    origin: "*"
};

//database pg pool
const db = require("./helper/dbhelper");

try {
    //database pg pool connection
    db.connect();
}
catch (err) {
    console.log('Error at database connection :::', err);
}




app.use(cors(corsOptions));
//for uploading file in submit api 
//app.use(fileUpload());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'resources')));
app.use(logger('dev'));
//app.use(express.urlencoded({ extended: false }));



//api routes
var api1 = require('./routes/api1')


//master routes
app.use('/api/v1.1', api1);




app.get("/", (req, res) => {
    res.json({ message: "Welcome to Api Application." });
});


// set port, listen for requests
const PORT = process.env.PORT || 8080;



// app.listen(PORT, '0.0.0.0', () => {
//     console.log(`Server is running on port ${PORT}.`);
// });


/**
 * Create HTTP server.
 */


var server = http.createServer(app);




server.listen(PORT);
server.on('error', onError);
server.on('listening', onListening);






/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
* Event listener for HTTP server "listening" event.
*/

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}








// Https server on port
// try {
// const httpsServer = https.createServer({
//   key: fs.readFileSync('/etc/letsencrypt/live/nkda.cca.distronix.in/privkey.pem'),
//   cert: fs.readFileSync('/etc/letsencrypt/live/nkda.cca.distronix.in/fullchain.pem'),
// }, app);

// httpsServer.listen(8081, () => {
//     console.log('HTTPS Server running on port 8081');
// });
// } catch (e) {
//   console.error(e.message);
// }




//Tracking Table Array
global.tracking_table = [];






//Load time table
async function loadTimeTable() {
    let query = `select * from time_table;`;
    //Routes finding query
    const loadTimeTable = (q) => {
        return new Promise((resolve, reject) => {
            db.query(q, (err, result) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }

                resolve(result.rows);
            })
        })
    }

    global.time_table = await loadTimeTable(query);
    console.log('time table ::: ',time_table);
}

loadTimeTable();


//Tracking Table Calculation
setInterval(function () {


    tracking_table.forEach(element => {
        let index = element.nextRoutes.indexOf(element.tracking);
        // console.log('index:::',index);
        if(index != -1)
        {
             for(var i=0;i<=index;i++)
             {
                 element.nextRoutes.shift();
             }
        }
        // console.log('nextRoutes :::',element.nextRoutes);
        
    });

}, 5000); // Execute in 5 sec time interval


//After reach last stop set trum deactive
setInterval(function () {


    tracking_table.forEach(element => {
        let nextRoutes = element.nextRoutes;
        if(nextRoutes.length === 0)
        {
            let removeVehicalNo = element.vehicleNo;
            let findIndex = tracking_table.findIndex(t => t.vehicleNo === removeVehicalNo);
            console.log('Index of deleted trum',findIndex);

            if(findIndex !== -1) {
                tracking_table.splice(findIndex , 1);
                console.error('Trum '+ removeVehicalNo + ' Removed from RAM');
            } 
        }
        
    });

}, 3000); // Execute in 5 sec time interval