function getDataArray(query) {
  return new Promise(function (resolve, reject) {
    db.query(query, function (err, result) {
      if (err) {
        console.log(err);
        reject(err);
      }

      resolve(result.rows);
    });
  });
}

const { getDistance, isPointWithinRadius } = require("geolib");

const fetch = require("node-fetch");

const db = require("../helper/dbhelper");

// const {latestTramDataArray, latestRouteDataArray} = require('../globalStaticTables');
const globalStaticTables = require("../globalStaticTables");

async function tramUpdateLocation(req, res) {
  // var queryForLatestRouteDataArray = "SELECT * FROM trum_route";
  // var latestRouteDataArray = await getDataArray(queryForLatestRouteDataArray);
  // console.log({ latestRouteDataArray });
  // var queryForLatestTramDataArray = "SELECT * FROM trum_stop";
  // var latestTramDataArray = await getDataArray(queryForLatestTramDataArray);
  //console.log({ latestTramDataArray });
  var {latestTramDataArray, latestRouteDataArray} = await globalStaticTables()

  var latestTramLocationData = await fetch(
    "https://transport.ideationts.com/app/vehicles/getTrams.json", //"http://tram.distronix.in:3000/tram",
    {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
    }
  )
    .then(async (response) => response.json())
    .catch((e) => {
      console.error("Error: ", e);
    });
  //console.log({ latestTramLocationData });
  // console.log("latestTramLocationData: ", latestTramLocationData.data);

  function measure(lat1, lng1, lat2, lng2, miles) {
    // miles optional
    //console.log({ lat1, lng1, lat2, lng2 });
    if (typeof miles === "undefined") {
      miles = false;
    }
    function deg2rad(deg) {
      return deg * (Math.PI / 180);
    }
    function square(x) {
      return Math.pow(x, 2);
    }
    var r = 6371; // radius of the earth in km
    lat1 = deg2rad(lat1);
    lat2 = deg2rad(lat2);
    var lat_dif = lat2 - lat1;
    var lng_dif = deg2rad(lng2 - lng1);
    var a =
      square(Math.sin(lat_dif / 2)) +
      Math.cos(lat1) * Math.cos(lat2) * square(Math.sin(lng_dif / 2));
    var d = 2 * r * Math.asin(Math.sqrt(a));
    if (miles) {
      return d * 0.000621371;
    } //return miles
    else {
      return d;
    } //return meters
  }

  //Calculating Stoppage ID for a single vehicle
  function trackingLocationStatusOfVehicle(
    vehicleNo,
    routeCode,
    latitude,
    longitude
  ) {
    var currentRouteStoppageIds = latestRouteDataArray.find(
      (route) => route.route_code === routeCode
    ).route;
    var currentRouteStoppages = currentRouteStoppageIds.map((id) =>
      latestTramDataArray.find((stoppage) => stoppage.stop_id === id)
    );
    //console.log({ currentRouteStoppages });
    //console.log("============================================================================================================================")
    // var distanceMatrixForCurrentRouteStoppages = currentRouteStoppages.map(
    //     (stoppage) => {

    //         return {
    //     stoppage: stoppage,
    //     distance: measure(
    //       latitude,
    //       longitude,
    //       parseFloat(stoppage.latitude),
    //       parseFloat(stoppage.longitude)
    //     ),
    //   }}
    // );
    // //   distanceMatrixForCurrentRouteStoppages.forEach(s => {
    //       console.log({stoppage: s.stoppage, distance: s.distance})
    //   })
    var distanceMatrixForCurrentRouteStoppages = currentRouteStoppages.map(
      (stoppage) => ({
        stoppage: stoppage,
        distance: getDistance(
          { latitude: latitude, longitude: longitude },
          {
            latitude: parseFloat(stoppage.latitude),
            longitude: parseFloat(stoppage.longitude),
          }
        ),
      })
    );
    var nearestStoppageFromTram = distanceMatrixForCurrentRouteStoppages.reduce(
      (p, n) => (n.distance <= p.distance ? n : p)
    );
    // console.log({ nearestStoppageFromTram });
    var nearestStoppageIdInRouteArray = currentRouteStoppageIds.findIndex(
      (stoppage_id) => stoppage_id === nearestStoppageFromTram.stoppage.stop_id
    );
    var secondNearestStoppageFromTram =
      nearestStoppageIdInRouteArray != 0 &&
      nearestStoppageIdInRouteArray != currentRouteStoppageIds.length - 1
        ? distanceMatrixForCurrentRouteStoppages[
            nearestStoppageIdInRouteArray - 1
          ].distance <
          distanceMatrixForCurrentRouteStoppages[
            nearestStoppageIdInRouteArray + 1
          ].distance
          ? distanceMatrixForCurrentRouteStoppages[
              nearestStoppageIdInRouteArray - 1
            ].stoppage
          : distanceMatrixForCurrentRouteStoppages[
              nearestStoppageIdInRouteArray + 1
            ].stoppage
        : nearestStoppageIdInRouteArray === 0
        ? currentRouteStoppages[1]
        : currentRouteStoppages[currentRouteStoppageIds.length - 2];
    // console.log({ secondNearestStoppageFromTram });
    var secondNearestStoppageIdInRouteArray = currentRouteStoppageIds.findIndex(
      (stoppage_id) => stoppage_id === secondNearestStoppageFromTram.stop_id
    );
    // var isTramInNearestStoppage =
    //   measure(
    //     parseFloat(nearestStoppageFromTram.stoppage.latitude),
    //     parseFloat(nearestStoppageFromTram.stoppage.longitude),
    //     latitude,
    //     longitude
    //   ) <= 100;
    var isTramInNearestStoppage = isPointWithinRadius(
      { latitude: latitude, longitude: longitude },
      {
        latitude: parseFloat(nearestStoppageFromTram.stoppage.latitude),
        longitude: parseFloat(nearestStoppageFromTram.stoppage.longitude),
      }
    );
    var nearestStoppageHasCrossed =
      nearestStoppageIdInRouteArray < secondNearestStoppageIdInRouteArray;
    return {
      vehicleNo: vehicleNo,
      routeCode: routeCode,
      current: isTramInNearestStoppage ? nearestStoppageFromTram : "travelling",
      next: nearestStoppageHasCrossed
        ? secondNearestStoppageFromTram
        : nearestStoppageFromTram,
      distance: nearestStoppageHasCrossed
        ? distanceMatrixForCurrentRouteStoppages[
            secondNearestStoppageIdInRouteArray
          ].distance
        : distanceMatrixForCurrentRouteStoppages[nearestStoppageIdInRouteArray]
          .distance,
    };
  }
  var latestTramLocationStoppageMappingData = latestTramLocationData.data.map(
    (tram) => {
      return trackingLocationStatusOfVehicle(
        tram.vehicleNo,
        tram.routeCode,
        tram.lastLocation.latitude,
        tram.lastLocation.longitude
      );
    }
  );
  // res.json(latestTramLocationStoppageMappingData);
  return latestTramLocationStoppageMappingData;
}
// export default tramUpdateLocation;

module.exports = tramUpdateLocation;




// function getDataArray(query) {
//   return new Promise(function (resolve, reject) {
//     db.query(query, function (err, result) {
//       if (err) {
//         console.log(err);
//         reject(err);
//       }

//       resolve(result.rows);
//     });
//   });
// }

// const { getDistance, isPointWithinRadius } = require("geolib");

// const fetch = require("node-fetch");

// const db = require("../helper/dbhelper");

// async function tramUpdateLocation(req, res) {
//   var queryForLatestRouteDataArray = "SELECT * FROM trum_route";
//   var latestRouteDataArray = await getDataArray(queryForLatestRouteDataArray);
//   //console.log({ latestRouteDataArray });
//   var queryForLatestTramDataArray = "SELECT * FROM trum_stop";
//   var latestTramDataArray = await getDataArray(queryForLatestTramDataArray);
//   //console.log({ latestTramDataArray });
//   var latestTramLocationData = await fetch(
//     "https://transport.ideationts.com/app/vehicles/getTrams.json",
//     {
//       method: "post",
//       body: JSON.stringify({}),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     }
//   )
//     .then(async (response) => response.json())
//     .catch((e) => {
//       console.error("Error: ", e);
//     });
//   //console.log({ latestTramLocationData });
//   console.log("latestTramLocationData: ", latestTramLocationData.data);

//   function measure(lat1, lng1, lat2, lng2, miles) {
//     // miles optional
//       //console.log({ lat1, lng1, lat2, lng2 });
//     if (typeof miles === "undefined") {
//       miles = false;
//     }
//     function deg2rad(deg) {
//       return deg * (Math.PI / 180);
//     }
//     function square(x) {
//       return Math.pow(x, 2);
//     }
//     var r = 6371; // radius of the earth in km
//     lat1 = deg2rad(lat1);
//     lat2 = deg2rad(lat2);
//     var lat_dif = lat2 - lat1;
//     var lng_dif = deg2rad(lng2 - lng1);
//     var a =
//       square(Math.sin(lat_dif / 2)) +
//       Math.cos(lat1) * Math.cos(lat2) * square(Math.sin(lng_dif / 2));
//     var d = 2 * r * Math.asin(Math.sqrt(a));
//     if (miles) {
//       return d * 0.000621371;
//     } //return miles
//     else {
//       return d;
//     } //return meters
//   }

//   //Calculating Stoppage ID for a single vehicle
//   function trackingLocationStatusOfVehicle(
//     vehicleNo,
//     routeCode,
//     latitude,
//     longitude
//   ) {
//     var currentRouteStoppageIds = latestRouteDataArray.find(
//       (route) => route.route_code === routeCode
//     ).route;
//     var currentRouteStoppages = currentRouteStoppageIds.map((id) =>
//       latestTramDataArray.find((stoppage) => stoppage.stop_id === id)
//     );
//       //console.log({ currentRouteStoppages });
//       //console.log("============================================================================================================================")
//     var distanceMatrixForCurrentRouteStoppages = currentRouteStoppages.map(
//         (stoppage) => {
//             // console.log(
//             //   "Stoppage Location: ",
//             //   parseFloat(stoppage.latitude),
//             //   parseFloat(stoppage.longitude)
//             // );
//             return {
//         stoppage: stoppage,
//         distance: measure(
//           latitude,
//           longitude,
//           parseFloat(stoppage.latitude),
//           parseFloat(stoppage.longitude)
//         ),
//       }}
//     );
//     //   distanceMatrixForCurrentRouteStoppages.forEach(s => {
//     //       console.log({stoppage: s.stoppage, distance: s.distance})
//     //   })
//     // var distanceMatrixForCurrentRouteStoppages = currentRouteStoppages.map(stoppage => ({ stoppage: stoppage, distance: getDistance({ latitude: latitude, longitude: longitude }, { latitude: parseFloat(stoppage.latitude), longitude: parseFloat(stoppage.longitude) }) }));
//     var nearestStoppageFromTram = distanceMatrixForCurrentRouteStoppages.reduce(
//       (p, n) => (n.distance <= p.distance ? n : p)
//     );
//     console.log({ nearestStoppageFromTram });
//     var nearestStoppageIdInRouteArray = currentRouteStoppageIds.findIndex(
//       (stoppage_id) => stoppage_id === nearestStoppageFromTram.stoppage.stop_id
//     );
//     var secondNearestStoppageFromTram =
//       nearestStoppageIdInRouteArray != 0 &&
//       nearestStoppageIdInRouteArray != currentRouteStoppageIds.length - 1
//         ? distanceMatrixForCurrentRouteStoppages[
//             nearestStoppageIdInRouteArray - 1
//           ].distance <
//           distanceMatrixForCurrentRouteStoppages[
//             nearestStoppageIdInRouteArray + 1
//           ].distance
//           ? distanceMatrixForCurrentRouteStoppages[
//               nearestStoppageIdInRouteArray - 1
//             ].stoppage
//           : distanceMatrixForCurrentRouteStoppages[
//               nearestStoppageIdInRouteArray + 1
//             ].stoppage
//         : nearestStoppageIdInRouteArray === 0
//         ? currentRouteStoppages[1]
//         : currentRouteStoppages[currentRouteStoppageIds.length - 2];
//     console.log({ secondNearestStoppageFromTram });
//     var secondNearestStoppageIdInRouteArray = currentRouteStoppageIds.findIndex(
//       (stoppage_id) => stoppage_id === secondNearestStoppageFromTram.stop_id
//     );
//     var isTramInNearestStoppage =
//       measure(
//         parseFloat(nearestStoppageFromTram.stoppage.latitude),
//         parseFloat(nearestStoppageFromTram.stoppage.longitude),
//         latitude,
//         longitude
//       ) <= 100;
//     var nearestStoppageHasCrossed =
//       nearestStoppageIdInRouteArray < secondNearestStoppageFromTram;
//       return {
//           vehicleNo: vehicleNo,
//           routeCode: routeCode,
//       current: isTramInNearestStoppage ? nearestStoppageFromTram : "travelling",
//       next: nearestStoppageHasCrossed
//         ? secondNearestStoppageFromTram
//         : nearestStoppageFromTram,
//     };
//   }
//   var latestTramLocationStoppageMappingData = latestTramLocationData.data.map(
//     (tram) =>
//       {
//          return trackingLocationStatusOfVehicle(
//            tram.vehicleNo,
//            tram.routeCode,
//            tram.lastLocation.latitude,
//            tram.lastLocation.longitude
//          );}
//   );
//   res.json(latestTramLocationStoppageMappingData);
// }
// // export default tramUpdateLocation;

// module.exports = tramUpdateLocation;
