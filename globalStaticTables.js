const db = require("./helper/dbhelper");

function getDataArray(query) {
    return new Promise(function (resolve, reject) {
      db.query(query, function (err, result) {
        if (err) {
          console.log(err);
          reject(err);
        }
  
        resolve(result.rows);
      });
    });
}

async function globalStaticTables(){
    var queryForLatestRouteDataArray = "SELECT * FROM trum_route";
    var latestRouteDataArray = await getDataArray(queryForLatestRouteDataArray);
//   console.log({ latestRouteDataArray });
    var queryForLatestTramDataArray = "SELECT * FROM trum_stop";
    var latestTramDataArray = await getDataArray(queryForLatestTramDataArray);

    return {latestRouteDataArray, latestTramDataArray};
}

module.exports = globalStaticTables;
